package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    //Indicar inici d'activitat en un test
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    //default test
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("cat.itb.cityquiz", appContext.getPackageName());
    }

    @Test
    public void startGame() {
        // button start
        onView(withId(R.id.startQuizBtn))
            .perform(click());
    }

    @Test
    public void quizQuestions() {
        //image quiz
        onView(withId(R.id.imageQuiz)).check(matches(isDisplayed()));
        //some buttons
        onView(withId(R.id.materialButton1)).perform(click());
        onView(withId(R.id.materialButton3)).perform(click());
        onView(withId(R.id.materialButton6)).perform(click());
    }

    public void endGame() {
        //score
        onView(withId(R.id.totalScore)).check(matches(isDisplayed()));
        onView(withId(R.id.playAgainButton)).perform(click());
    }
}
