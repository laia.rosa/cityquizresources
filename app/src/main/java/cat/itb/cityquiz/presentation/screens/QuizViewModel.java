package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    MutableLiveData<Game> liveData = new MutableLiveData<>();

    public void startQuiz() {
        //primer crea joc
        Game game = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
        liveData.postValue(game);
    }

    public MutableLiveData<Game> getGame() {
        return liveData;
    }

    public void answerQuestion(int answer) {
        Game game = gameLogic.answerQuestions(liveData.getValue(), answer);
        liveData.postValue(game);
    }
}
