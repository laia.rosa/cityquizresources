package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class StartQuizFragment extends Fragment {

    private QuizViewModel mViewModel;

    public static StartQuizFragment newInstance() {
        return new StartQuizFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_quiz_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button button = view.findViewById(R.id.startQuizBtn);

        button.setOnClickListener(this::startQuiz);
    }

    private void startQuiz(View view) {
        mViewModel.startQuiz();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        mViewModel.getGame().observe(this, this::onGameChanged);
    }

    private void onGameChanged(Game game) {
        if (game != null) {
            Navigation.findNavController(getView()).navigate(R.id.action_start_quiz);
        }
    }
}
