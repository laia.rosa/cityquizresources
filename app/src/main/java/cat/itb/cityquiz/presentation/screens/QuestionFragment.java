package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;

public class QuestionFragment extends Fragment {

    private QuizViewModel mViewModel;
    Button button1, button2, button3, button4, button5, button6;
    ImageView imageView;

    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button1 = getView().findViewById(R.id.materialButton1);
        button2 = getView().findViewById(R.id.materialButton2);
        button3 = getView().findViewById(R.id.materialButton3);
        button4 = getView().findViewById(R.id.materialButton4);
        button5 = getView().findViewById(R.id.materialButton5);
        button6 = getView().findViewById(R.id.materialButton6);

        imageView = getView().findViewById(R.id.imageQuiz);

        button1.setOnClickListener(this::questionAnswered);
        button2.setOnClickListener(this::questionAnswered);
        button3.setOnClickListener(this::questionAnswered);
        button4.setOnClickListener(this::questionAnswered);
        button5.setOnClickListener(this::questionAnswered);
        button6.setOnClickListener(this::questionAnswered);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        mViewModel.getGame().observe(this, this::onGameChanged);
    }

    private void onGameChanged(Game game) {
        display(game);
    }

    private void display(Game game) {
        if(game.isFinished()) {
            Navigation.findNavController(getView()).navigate(R.id.action_finish_quiz);
        } else {
            List<City> cities = game.getCurrentQuestion().getPossibleCities();

            button1.setText(cities.get(0).getName());
            button2.setText(cities.get(1).getName());
            button3.setText(cities.get(2).getName());
            button4.setText(cities.get(3).getName());
            button5.setText(cities.get(4).getName());
            button6.setText(cities.get(5).getName());

            City correctCity = game.getCurrentQuestion().getCorrectCity();

            String fileName = ImagesDownloader.scapeName(correctCity.getName());
            int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
            imageView.setImageResource(resId);
        }
    }

    private void questionAnswered(View view) {
        switch (view.getId()){
            case R.id.materialButton1:
                mViewModel.answerQuestion(0);
                break;
            case R.id.materialButton2:
                mViewModel.answerQuestion(1);
                break;
            case R.id.materialButton3:
                mViewModel.answerQuestion(2);
                break;
            case R.id.materialButton4:
                mViewModel.answerQuestion(3);
                break;
            case R.id.materialButton5:
                mViewModel.answerQuestion(4);
                break;
            case R.id.materialButton6:
                mViewModel.answerQuestion(5);
                break;
        }
    }
}
